
# OpenEMR Report Generation
___
## Report Formats:
* CAMOS: https://www.open-emr.org/wiki/index.php/CAMOS_module
* LBV Forms: https://www.open-emr.org/wiki/index.php/LBV_Forms
* Nation Notes: https://www.open-emr.org/wiki/index.php/Nation_Notes
___
* Prescription Generation
````
  - Patient/Client ‣ Patients( Select targeted patient) ‣ Dashboard ‣ Prescription ‣ Edit ‣ Add ‣ Select Prescriptions ‣ Download PDF
````
* CAMOS Prescription
````
  - Patient/Client ‣ Patients( Select targeted patient) ‣ Dashboard ‣ Prescription ‣ Edit ‣ view four panel ‣ Select Prescriptions, Update User info ‣ print html
````
* Add report
````
  - Patient/Client ‣ Patients( Select targeted patient) ‣ Report ‣ View past encounters/ Open encounter ‣ Administrative ‣ Procedure order
````
* Report Generation
````
  - Patient/Client ‣ Patients( Select targeted patient) ‣ Report ‣ Patient Report(select options, Encounters & Forms) ‣ Generate report/ download PDF
````
* Report generation with CAMOS
````
  - Patient/Client ‣ Patients( Select targeted patient) ‣ Report ‣ View past encounters/ Open encounter ‣ Miscellaneous ‣ Camos ‣ prescription ‣ add content ‣ submit all content ‣ go to summary ‣ print this encounter
````
___
## OpenEMR Reports customization

### Link to follow:
* https://sourceforge.net/p/openemr/discussion/202505/thread/1b694a96/#7dad/
# Billing & Configuration
___
## Links studied-

* Configuration- https://www.open-emr.org/wiki/index.php/Basic_Billing_Setup_and_Configuration
* Howtos- https://www.open-emr.org/wiki/index.php/OpenEMR_Billing_Setup_Howtos
* Billing- https://www.open-emr.org/wiki/index.php/Basic_Billing
